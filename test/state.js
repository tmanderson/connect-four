var script = document.createElement('script');
script.setAttribute('src', '../src/state.js');
document.body.appendChild(script);

function b(number) {
	return number.toString(2);
}

function addIntervals(list, start, i, val) {
	var iter = 4;
	list = [].concat(list);
	while(iter--) {
		if(start+i*iter < list.length) list[start+i*iter] = val;
	}
	return list;
}

script.onload = function() {
	var state = new C4.GameState();

	//	If only JS used 64-bit ints...
	function testInterval(name, interval) {
		var empty = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			board,
			i = 0,
			pass = 0,
			fail = 0;

		console.groupCollapsed(name);
		while(i < 42) {
			state.cells = addIntervals(empty, i++, interval, 1);
			
			console.log(state.isComplete())
			_.each(_.range(6), function(j) {
				console.log(state.cells.slice(j*7,j*7+7).join(''));
			});
			console.log('\n')
			state.isComplete() ? pass++ : fail++;
		}
		console.groupEnd(name);
		console.log('PASSED: ' + pass, 'FAILED: ' + fail);
	}

	testInterval('horizontal', 1);
	testInterval('diagonal', 6);
	testInterval('diagonal', 8);
	testInterval('vertical', 7);
};
