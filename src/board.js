(function(scope) {
	'use strict';

	(scope = scope || window).C4 = scope.C4 || {};

	var el;

	/**
	 *	ConnectFourGame (constructor)
	 *	controller logic between game state and UI.
	 */
	function ConnectFourGame(state) {
		var $row;

		el = C4.utils.el;

		//	If no game state provided, make one 7x6 state
		if(!state) state = new C4.GameState(7,6);
		this.state = state;

		//	create and set the element (jquery wrapped)
		this.$el = $( el('div') ).addClass('board');

		//	create our rows and cells (all divs)
		this.state.each(function(value, i) {
			var cell;

			if(!(i%this.state.columns)) {
				$row = $(el('div')).addClass('row')
							.appendTo(this.$el);
			}

			cell = $(el('div')).addClass('cell');
			$row.append(cell);
		}, this);

		//	convenience properties
		this.$rows = this.$el.find('.row');
		this.$cells = this.$el.find('.cell');

		//	default to player 1
		this.activePlayer = 1;
		this.playerNames = ['Player 1', 'Player 2'];

		this.addListeners();
	}

	ConnectFourGame.prototype = {
		playerNames: null,

		/**
		 *	setPlayer
		 *	@i - player number (1 or 2)
		 *	@name - player name
		 *	set a player's name
		 */
		setPlayer: function(name, i) {
			if(_.isArray(name)) return _.each(name, this.setPlayer, this);
			this.playerNames[i] = name || this.playerNames[i];
		},

		/**
		 *	dropPiece
		 *	when a player clicks a column, the piece sinks to the lowest place
		 *	it can fall, this simulates that.
		 */
		dropPiece: function(e) {
			var $target 	= $(e.target),
				col 		= $target.index(),
				row,
				cell;

			this.$el.attr('class', 'board');

			cell = this.state.drop( col, this.activePlayer );
			row = this.$cells.eq(cell).parent().index();

			this.$el.attr('data-player', this.activePlayer);
			this.animateDrop(col, row, cell);

			if(this.state.winningCells) {
				this.animateWinners(this.state.winningCells);
				this.$el.trigger('player-win', this.playerNames[this.activePlayer-1]);
				this.$el.off();
				return;
			}

			this.togglePlayer();
		},

		/**
		 *	togglePlayer
		 *	toggles between player values
		 */
		togglePlayer: function() {
			this.activePlayer = (this.activePlayer === 1) ? 2 : 1;
			this.$el.trigger('next-player', this.playerNames[this.activePlayer-1]);
		},

		/**
		 *	animateWinners
		 *	@winners (array) - array of cells with winning pieces
		 *	these are staggared at first (when they scale), so using a timeout
		 *	to stagger 150ms between each scale.
		 */
		animateWinners: function(winners) {
			var i, curCell;

			for(i = 0; i < winners.length; i++) {
				curCell = this.$cells.eq(winners[i]);
				setTimeout(_.bind(curCell.addClass, curCell, 'winner'), 150+150*i);
			}
		},

		/**
		 *	animateDrop
		 *	@col (number) - the column number to animate
		 *	@row (number) - the row to end the animation at
		 *	@cell (number) - the final cell
		 *	the drop animation is staggared, this takes care of the drop.
		 */
		animateDrop: function(col, row, cell) {
			this.unlisten();

			this.$rows.slice(0, row).map(function() {
				return this.childNodes[col];
			}).each(function(i) {
				setTimeout(_.bind(this.classList.add, this.classList, 'drop'), 150*i);
			});

			setTimeout(_.bind(this.clearDrops, this, cell), 150*row);
		},

		/**
		 *	clearDrops
		 * 	cleans up remaining classes from drop animation, adds final param
		 * 	to the taken cell.
		 */
		clearDrops: function(cell) {
			this.$cells.removeClass('drop')
				.eq(cell).attr( 'data-player', this.$el.attr('data-player') );

			if(!this.$el.find('.winner').length) this.addListeners();
		},

		/**
		 *	addListeners
		 *	delegate click listener for dropping pieces
		 */
		addListeners: function() {
			this.$el.on('click', _.bind(this.dropPiece, this));
		},

		unlisten: function() {
			this.$el.off('click');
		}
	};

	C4.Game = ConnectFourGame;

})();