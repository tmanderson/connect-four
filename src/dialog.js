(function(scope) {

	(scope = scope || window).C4 = scope.C4 || {};

	function Dialog() {
		var el = C4.utils.el;

		this.$el = $('.dialog');
		this.$input = this.$el.find('input');

		this.$input.on('keydown', _.bind(this.handleKey, this));
		this.$el.on('click', 'button', _.bind(this.submit, this));
	}

	Dialog.prototype = {
		setTitle: function(text) {
			this.$el.find('h2').text(text);
		},

		setBody: function(text) {
			this.$el.find('h3').text(text);
		},

		handleKey: function(e) {
			if(e.which === 13) this.submit();
		},

		submit: function() {
			this.$el.trigger('submit', this.$input.val());
			this.close();
		},

		show: function(title, body) {
			this.setTitle(title || '');
			this.setBody(body || '');

			this.$input.show().val('');
			this.$el.addClass('show');
			
			setTimeout(_.bind(this.$el.addClass, this.$el, 'in'), 50);
			setTimeout(_.bind(this.$input.focus, this.$input), 250);
		},

		showMessage: function(message) {
			this.show(message);
			this.$input.hide();
		},

		close: function() {
			this.$el.removeClass('in').addClass('out');
			setTimeout(_.bind(this.$el.attr, this.$el, 'class', 'dialog'), 300);
		}
	};

	C4.dialog = new Dialog();

})();