(function(scope) {
	'use strict';

	(scope = scope || window).C4 = scope.C4 || {};

	C4.utils = {
		el: function(tagName) {
			return document.createElement(tagName);
		}
	};

})();