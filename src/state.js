(function(scope) {
	'use strict';

	(scope = scope || window).C4 = scope.C4 || {};

	/**
	 *	GameState (constructor)
	 * 	@c (number) 	- the number of columns per row
	 *	@r (number) 	- the number of rows to create
	 *	@toWin (number) - number of pieces in a row needed to win (default = 4)
	 */
	function GameState(c, r, toWin) {
		c = c || 7;
		r = r || 6;

		var cells = c * r;

		this.cells = [];
		while(cells--) this.cells.push(0);

		this.rows = r;
		this.columns = c;
		this.player = 1;

		if(toWin) this.win = toWin;
	}

	GameState.fromArray = function(c, r, arr) {
		var gs = new GameState(c, r),
			i = 0;

		while(i < gs.length) gs[i] = arr[i++];
		return gs;
	};

	GameState.prototype = _.extend(Object.create(Array.prototype), {
		cells: null,
		rows: 0,
		cols: 0,
		win : 4,

		/**
		 *	drop
		 *	@c (number) 	- the column to drop a "piece"
		 *	@player (mixed) - the value that acts as an identifier for the player
		 *						(e.g. 0, 1, "player1", "john")
		 *
		 *	returns (number) 	- the cell the "piece" was placed in, -1 if the game has been won
		 */
		drop: function(c, player) {
			var cell;

			if(this.winningCells) return -1;

			this.player = player;

			this.each(function(val, i) {
				if(val) return;
				if(i%this.columns === c) cell = i;
			});

			this.cells[cell] = this.player;
			this.isComplete();

			return cell;
		},

		/**
		 *	isComplete
		 *	look for a winner!
		 *	returns true if there's a winner.
		 */
		isComplete: function() {
			var c = this.columns;
			//	loop until we find a cell with a value...
			for(var i = 0; i < this.cells.length; i++) {
				//	once we find a value, look for some other 3 values linked
				// 	horizontally, vertically, or diagonally
				if(this.cells[i] === this.player) {
					//	checks in the order of horzontal, vertical,
					// 	l-r diagonal, r-l diagonal
					if(this.isWinningLink(i, 1, 0) ||
						this.isWinningLink(i, c, 1) ||
							this.isWinningLink(i, c+1, 1) ||
								this.isWinningLink(i, c-1, 1)) return true;
				}
			}

			return false;
		},

		/**
		 *	isWinningLink
		 *	@cell (number) 		- The cell to begin our search from
		 *	@interval (number) 	- The number of cells between each cell that would
		 *							make this a valid win streak
		 *	@val (number)		- The value that a cell must match
		 *
		 *	Given a cell index an interval and a rowspan (0,1), we can determine
		 *	any valid win streak based on the first cell's index and a set interval.
		 * 	
		 *	Horizontal 		- n + 1
		 *	Vertical 		- n + columns
		 *	L-R Diagonal 	- n + (columns + 1)
		 *	R-L Diagonal	- n + (columns - 1)
		 *
		 * 	returns (boolean) - if the given cell is the head of a valid winning
		 *						streak
		 */
		isWinningLink: function(cell, interval, rowspan) {
			var cells = [ cell ],
				lastRow = Math.floor(cell/7),	// the row of the last valid cell
				curRow,							// the current row
				c,
				i;
			
			//	start at 1 as we already know we have the first match in `cell`
			for(i = 1; i < this.win; i++) {
				c = cell+interval*i;
				curRow = Math.floor(c/7);
				//	if this cell doesn't have the active players piece,
				// 	get outta' here
				
				if(this.cells[c] !== this.player) return false;
				//	if rows greater than specified span, this is no good
				if(Math.abs(curRow - lastRow) !== rowspan) return false;
				lastRow = curRow;
				//	add the cell's index to what may be the `winningCells`
				cells.push(cell+interval*i);
			}
			
			this.winningCells = cells;
			return true;
		},

		/**
		 * 	each
		 * 	convenience iterator
		 */
		each: function(fn, context) {
			_.each(this.cells, fn, context || this);
		}
	});

	C4.GameState = GameState;

})();