(function(scope) {
	'use strict';

	(scope = scope || window).C4 = scope.C4 || {};

	if(!C4.Game) console.error("We can't play without the game!");

	var players = [],
		currentGame;

	function startGame() {
		if(players.length !== 2) {
			getPlayers();
		}
		else {
			currentGame = new C4.Game();
			currentGame.setPlayer(players);
			players = currentGame.playerNames;

			$('.players h3').eq(0).text(players[0])
				.next().text(players[1]);

			$('.dialog').after(currentGame.$el);

			currentGame.$el.on('next-player', _.debounce(showPlayerMessage, 500));
			currentGame.$el.on('player-win', _.debounce(showWinnerMessage, 500));
		}
	}

	function showPlayerMessage(e, playerName) {
		C4.dialog.showMessage(playerName + '\'s turn');
	}

	function showWinnerMessage(e, playerName) {
		C4.dialog.showMessage(playerName + ' wins!');
	}

	function getPlayers() {
		var player = 'Player ' + (players.length+1),
			message = 'Enter ' + player + '\'s name';

		C4.dialog.show(player, 'Enter Player 1\'s name');
		C4.dialog.$el.one('submit', addPlayer);
	}

	function addPlayer(e, player) {
		players.push(escape(player));

		if(players.length < 2) {
			setTimeout(getPlayers, 400);
		}
		else {
			startGame();
			setTimeout(_.partial(showPlayerMessage, null, players[0]), 400);
		}
	}


	startGame();
})();